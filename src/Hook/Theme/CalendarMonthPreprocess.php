<?php

declare(strict_types=1);

namespace Drupal\calendar_render_element\Hook\Theme;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides calendar mouth preprocess.
 */
final readonly class CalendarMonthPreprocess implements ContainerInjectionInterface {

  /**
   * Constructs a new CalendarMonthPreprocess instance.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected TimeInterface $time,
  ) {}

  #[\Override]
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get(ConfigFactoryInterface::class),
      $container->get(TimeInterface::class),
    );
  }

  /**
   * Prepares variables for calendar month templates.
   */
  public function __invoke(array &$variables): void {
    $first_day_week = $this->configFactory->get('system.date')->get('first_day');

    $current_date = DrupalDateTime::createFromTimestamp($this->time->getCurrentTime());

    $year = $variables['year'] ?? $current_date->format('Y');
    $mouth = $variables['month'] ?? $current_date->format('m');
    $mouth = \ltrim($mouth, '0');
    $start_date = new \DateTimeImmutable("{$year}-{$mouth}-01");

    $dayDiff = (int) $start_date->format('N') - (int) $first_day_week;
    $dayDiff = $dayDiff < 0 ? $dayDiff + 7 : $dayDiff;

    $start_date = $start_date->modify('- ' . $dayDiff . ' days');
    $end_date = $start_date->modify('+ 42 days');

    $interval = new \DateInterval('P1D');
    $period = new \DatePeriod($start_date, $interval, $end_date);

    foreach ($period->getIterator() as $day) {
      $variables['days'][] = [
        '#theme' => 'calendar_mouth_day',
        '#number' => $day->format('j'),
        '#content' => $variables['content'][$day->format('Y-m-d')] ?? NULL,
        '#current' => $day->format('Y-m-d') === $current_date->format('Y-m-d'),
      ];
    }

    $variables['header'] = DateHelper::weekDaysOrdered(DateHelper::weekDaysAbbr(TRUE));
    $variables['month_name'] = DateHelper::monthNames(TRUE)[$mouth];

    $cacheable_metadata = CacheableMetadata::createFromRenderArray($variables);
    $cacheable_metadata->addCacheTags(['config:system.date']);
    $cacheable_metadata->applyTo($variables);
  }

}
