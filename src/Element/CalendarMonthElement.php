<?php

declare(strict_types=1);

namespace Drupal\calendar_render_element\Element;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Render\Element\ElementInterface;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a calendar month render element.
 *
 * Properties:
 * - #year: The year of the calendar month displayed will default to the current
 *   year.
 * - #month: The calendar month displayed will default to the current month.
 * - #variant: The convenient way to add a theme suggestion.
 * - #content: The array of data in which the key is the date (Y-m-d) of the day
 *   the value can be a string or render array.
 *
 * @code
 * $build['calendar'] = [
 *   '#type' => 'calendar_month',
 *   '#year' => '2024',
 *   '#month' => '01',
 *   '#variant' => 'simple',
 *   '#content' => [
 *     '2024-01-01' => 'New Year!',
 *     '2024-01-15' => 'Drupal birthday',
 *   ],
 * ];
 * @endcode
 *
 * @RenderElement("calendar_month")
 */
final class CalendarMonthElement extends PluginBase implements ElementInterface {

  #[\Override]
  public function getInfo(): array {
    return [
      '#theme' => 'calendar_month',
      '#year' => NULL,
      '#month' => NULL,
      '#variant' => NULL,
      '#content' => [],
      '#pre_render' => [
        [self::class, 'preRenderCalendar'],
      ],
    ];
  }

  #[\Override]
  public static function setAttributes(&$element, $class = []): void {
    // Remove after Drupal Core 10.2 support has been drop.
    // @phpstan-ignore-next-line
    RenderElement::setAttributes($element, $class);
  }

  /**
   * Prepares a #type 'calendar_month' element for calendar-month.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *
   * @return array
   *   The $element with prepared variables ready for calendar-month.html.twig.
   */
  public static function preRenderCalendar(array $element): array {
    if (!isset($element['#variant'])) {
      return $element;
    }

    $clean_variant = \str_replace([' ', '-'], '_', $element['#variant']);
    $clean_variant = \mb_strtolower($clean_variant);

    $element['#theme'] = [
      $element['#theme'] . '__' . $clean_variant,
      $element['#theme'],
    ];

    return $element;
  }

}
