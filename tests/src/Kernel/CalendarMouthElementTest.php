<?php

declare(strict_types=1);

namespace Drupal\Tests\calendar_render_element\Kernel;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateHelper;
use Drupal\KernelTests\KernelTestBase;

/**
 * Provide calendar month element test.
 */
final class CalendarMouthElementTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'calendar_render_element'];

  #[\Override]
  protected function setUp(): void {
    parent::setUp();

    $time_mock = $this->createMock(TimeInterface::class);
    // 01.01.2021.
    $time_mock->method('getCurrentTime')->willReturn(1609459200);
    $this->container->set('datetime.time', $time_mock);
  }

  /**
   * Tests calendar month element.
   *
   * @dataProvider calendarMouthElementProvider
   */
  public function testCalendarMouthElement(string $year, string $month_name, string $first_week_day, string $first_display_day, string $last_display_day, bool $display_current_day, array $content): void {
    $month = (string) \array_flip(DateHelper::monthNamesUntranslated())[$month_name];
    $first_week_day = \array_flip(DateHelper::weekDaysUntranslated())[$first_week_day];
    $first_week_day_name = (string) DateHelper::weekDaysAbbr(TRUE)[$first_week_day];

    $this->config('system.date')
      ->set('first_day', $first_week_day)
      ->save();

    $element = [
      '#type' => 'calendar_month',
      '#year' => $year,
      '#month' => $month,
      '#content' => \array_column($content, 'value', 'date'),
    ];

    $this->render($element);

    $result = $this->xpath('//h2[contains(@class, "calendar-month__month-name")][text()=:month]', [':month' => $month_name]);
    self::assertCount(1, $result);
    $result = $this->xpath('//div[contains(@class, "calendar-month__header")]/div[1][contains(@class, "calendar-month__header-day-name")][text()=:day]', [':day' => $first_week_day_name]);
    self::assertCount(1, $result);
    $result = $this->xpath('//div[contains(@class, "calendar-month__days")]/*');
    self::assertCount(42, $result);
    $result = $this->xpath('//div[contains(@class, "calendar-month__days")]/*[1]/div[contains(@class, "calendar-mouth-day__number")][text()=:number]', [':number' => $first_display_day]);
    self::assertCount(1, $result);
    $result = $this->xpath('//div[contains(@class, "calendar-month__days")]/*[42]/div[contains(@class, "calendar-mouth-day__number")][text()=:number]', [':number' => $last_display_day]);
    self::assertCount(1, $result);
    $result = $this->xpath('//div[contains(@class, "calendar-mouth-day--current")]');
    self::assertEquals($display_current_day, \count($result));

    foreach ($content as ['value' => $value, 'display' => $display]) {
      $result = $this->xpath('//div[text()=:value]', [':value' => $value]);
      self::assertEquals($display, \count($result));
    }
  }

  /**
   * Provides calendar mouth data.
   *
   * @return \Generator
   *   The data.
   */
  public function calendarMouthElementProvider(): \Generator {
    yield [
      '2024',
      'January',
      'Monday',
      '1',
      '11',
      FALSE,
      [
        ['value' => 'New Year', 'date' => '2024-01-01', 'display' => TRUE],
        ['value' => 'Note 1', 'date' => '2024-02-10', 'display' => TRUE],
      ],
    ];
    yield [
      '2001',
      'December',
      'Sunday',
      '25',
      '5',
      FALSE,
      [
        ['value' => 'Note 1', 'date' => '2001-11-25', 'display' => TRUE],
        ['value' => 'Note 2', 'date' => '2001-12-31', 'display' => TRUE],
      ],
    ];
    yield [
      '2021',
      'January',
      'Sunday',
      '27',
      '6',
      TRUE,
      [
        ['value' => 'Note 1', 'date' => '2021-01-01', 'display' => TRUE],
        ['value' => 'Note 2', 'date' => '2021-02-10', 'display' => FALSE],
      ],
    ];
  }

}
